%global url_ver %%(echo %{version} | cut -d. -f1-2)

Summary: A low-level core library forms the basis for projects such as GTK and GNOME.
Name: glib2
Version: 2.78.3
Release: 7%{?dist}
License: LGPLv2+
URL: https://docs.gtk.org/glib/
Source0: https://download.gnome.org/sources/glib/%{url_ver}/glib-%{version}.tar.xz
Source1: glib-run-installed-tests

Patch0001:     CVE-2024-52533-gsocks4aproxy-Fix-a-single-byte-buffer-overflow-in-c.patch
Patch0002:     https://github.com/GNOME/glib/commit/ef064d625b847e05a017fcc307e67c6f31880ce7.patch

BuildRequires: gcc, gcc-c++, meson, perl-interpreter
BuildRequires: gettext, xsltproc, docbook-xsl, gtk-doc
BuildRequires: glibc-devel, libattr-devel, libselinux-devel, systemtap-sdt-devel, python3-devel
BuildRequires: pkgconfig(libelf), pkgconfig(libffi), pkgconfig(libpcre2-8)
BuildRequires: pkgconfig(mount), pkgconfig(sysprof-capture-4), pkgconfig(zlib)
BuildRequires: /usr/bin/marshalparser

# for tests
BuildRequires:  shared-mime-info
BuildRequires:  /usr/bin/dbus-daemon
BuildRequires:  /usr/bin/update-desktop-database

%description
GLib is a general-purpose, portable utility library, which
provides many useful data types, macros, type conversions,
string utilities, file utilities, a mainloop abstraction,
and so on.


%package devel
Summary: A library of handy utility functions
Requires: %{name} = %{version}-%{release}

%description devel
This package provides header files of the GLib library for developers.

%package static
Summary: glib static
Requires: %{name}-devel = %{version}-%{release}
Requires: pcre2-static
Requires: sysprof-capture-static

%description static
This package provides static libraries of GLib.

%package doc
Summary: A library of handy utility functions
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
The glib2-doc package includes documentation for the GLib library.

%package tests
Summary: Tests for the glib2 package
Requires: %{name} = %{version}-%{release}

%description tests
This package provides some tests which can be used to check the
installed glib2 package.

%prep
%autosetup -n glib-%{version} -p1

%build
%meson \
    -Dman=true \
    -Ddtrace=true \
    -Dsystemtap=true \
    -Dsysprof=enabled \
    -Dglib_debug=disabled \
    -Dgtk_doc=true \
    -Dinstalled_tests=true \
    --default-library=both \
    %{nil}

%meson_build

%install
%meson_install

touch -r gio/gdbus-2.0/codegen/config.py.in %{buildroot}%{_datadir}/glib-2.0/codegen/*.py
%global py_reproducible_pyc_path %{buildroot}%{_datadir}
%py_byte_compile %{python3} %{buildroot}%{_datadir}

mkdir -p %{buildroot}%{_libdir}/gio/modules
install -Dp -m755 %{SOURCE1} %{buildroot}%{_libexecdir}/installed-tests/

%find_lang glib20

%check
%meson_test

%transfiletriggerin -- %{_libdir}/gio/modules
gio-querymodules %{_libdir}/gio/modules &> /dev/null || :

%transfiletriggerpostun -- %{_libdir}/gio/modules
gio-querymodules %{_libdir}/gio/modules &> /dev/null || :

%transfiletriggerin -- %{_datadir}/glib-2.0/schemas
glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :

%transfiletriggerpostun -- %{_datadir}/glib-2.0/schemas
glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :

%files -f glib20.lang
%license COPYING
%doc NEWS README.md
%{_libdir}/libglib-2.0.so.*
%{_libdir}/libgthread-2.0.so.*
%{_libdir}/libgmodule-2.0.so.*
%{_libdir}/libgobject-2.0.so.*
%{_libdir}/libgio-2.0.so.*
%dir %{_libdir}/gio
%{_libdir}/gio/modules
%ghost %{_libdir}/gio/modules/giomodule.cache
%dir %{_datadir}/bash-completion
%dir %{_datadir}/bash-completion/completions
%{_datadir}/bash-completion/completions/gapplication
%{_datadir}/bash-completion/completions/gdbus
%{_datadir}/bash-completion/completions/gio
%{_datadir}/bash-completion/completions/gsettings
%dir %{_datadir}/glib-2.0
%dir %{_datadir}/glib-2.0/schemas
%{_bindir}/gio
%{_bindir}/gio-querymodules
%{_libexecdir}/gio-launch-desktop
%{_bindir}/glib-compile-schemas
%{_bindir}/gsettings
%{_bindir}/gdbus
%{_bindir}/gapplication
%{_mandir}/man1/gio.1*
%{_mandir}/man1/gio-querymodules.1*
%{_mandir}/man1/glib-compile-schemas.1*
%{_mandir}/man1/gsettings.1*
%{_mandir}/man1/gdbus.1*
%{_mandir}/man1/gapplication.1*

%files devel
%{_libdir}/lib*.so
%{_libdir}/glib-2.0
%{_libdir}/pkgconfig/*
%{_includedir}/*
%{_datadir}/aclocal/*
%{_datadir}/glib-2.0/dtds
%{_datadir}/glib-2.0/gdb
%{_datadir}/glib-2.0/gettext
%{_datadir}/glib-2.0/schemas/gschema.dtd
%{_datadir}/glib-2.0/valgrind/glib.supp
%{_datadir}/bash-completion/completions/gresource
%{_datadir}/glib-2.0/codegen
%{_datadir}/gdb/
%{_datadir}/gettext/
%{_datadir}/systemtap/
%{_bindir}/glib-genmarshal
%{_bindir}/glib-gettextize
%{_bindir}/glib-mkenums
%{_bindir}/gobject-query
%{_bindir}/gtester
%{_bindir}/gdbus-codegen
%{_bindir}/glib-compile-resources
%{_bindir}/gresource
%attr (0755, root, root) %{_bindir}/gtester-report
%{_mandir}/man1/glib-genmarshal.1*
%{_mandir}/man1/glib-gettextize.1*
%{_mandir}/man1/glib-mkenums.1*
%{_mandir}/man1/gobject-query.1*
%{_mandir}/man1/gtester-report.1*
%{_mandir}/man1/gtester.1*
%{_mandir}/man1/gdbus-codegen.1*
%{_mandir}/man1/glib-compile-resources.1*
%{_mandir}/man1/gresource.1*

%files static
%{_libdir}/libgio-2.0.a
%{_libdir}/libglib-2.0.a
%{_libdir}/libgmodule-2.0.a
%{_libdir}/libgobject-2.0.a
%{_libdir}/libgthread-2.0.a

%files doc
%{_datadir}/gtk-doc/

%files tests
%{_libexecdir}/installed-tests
%{_datadir}/installed-tests

%changelog
* Fri Feb 14 2025 Shuo Wang <abushwang@tencent.com> - 2.78.3-7
- revert 092fedd5f085a2f1966b5c34befe8b603c1a0f07.patch to fix reboot error

* Mon Dec 30 2024 Tracker Robot <trackbot@opencloudos.tech> - 2.78.3-6
- [Type] bugfix
- [DESC] Apply patches from rpm-tracker
- [Bug Fix] ef064d625b847e05a017fcc307e67c6f31880ce7.patch: gopenuriportal: Fix two memory leaks
- [Bug Fix] 092fedd5f085a2f1966b5c34befe8b603c1a0f07.patch: gdbus: Fix leak of method invocation when registering an object with closures

* Mon Nov 18 2024 Shuo Wang <abushwang@tencent.com> - 2.78.3-5
- gsocks4aproxy: Fix a single byte buffer overflow in connect messages

* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.78.3-4
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.78.3-3
- Rebuilt for loongarch release

* Wed May 29 2024 Shuo Wang <abushwang@tencent.com> - 2.78.3-2
- enable %check

* Thu Dec 28 2023 Shuo Wang <abushwang@tencent.com> - 2.78.3-1
- update to 2.78.3

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.77.1-2
- Rebuilt for OpenCloudOS Stream 23.09

* Wed Aug 9 2023 Wang Guodong <gordonwwang@tencent.com> - 2.77.1-1
- Upgrade to 2.77.1

* Tue Jul 25 2023 kianli <kianli@tencent.com> - 2.77.0-1
- Upgrade to 2.77.0

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.72.1-5
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.72.1-4
- Rebuilt for OpenCloudOS Stream 23

* Wed Nov 30 2022 cunshunxia <cunshunxia@tencent.com> - 2.72.1-3
- fix anaconda install error

* Thu Nov 17 2022 Zhao Zhen <jeremiazhao@tencent.com> - 2.72.1-2
- recovered document subpackage

* Tue May 17 2022 cunshunxia <cunshunxia@tencent.com> - 2.72.1-1
- initial build
